class LinkedList:
    def __init__(self, label, next=None):
        self.label = label
        self.next = next
        self.prev = None
        
def get_label(first_node):
    print_str = ""
    node = first_node
    while node:
        print_str += node.label
        node = node.next
    return print_str

def search_word(head_node, next_node, prev_node=None):
    if next_node == prev_node:
        print("Symmetrical:" + get_label(head_node))
        return
    elif next_node.label != prev_node.label:
        print("Not Symmetrical:" + get_label(head_node))
        return
    else:
        search_word(head_node, next_node.next, prev_node.prev)

def create_linked_list(str):
    before_linked_list = None
    last_node = None
    for s in str[::-1]:
        linked_list = LinkedList(s, before_linked_list)
        if before_linked_list == None:
            last_node = linked_list
        else:
            before_linked_list.prev =  linked_list 
        before_linked_list = linked_list
    return before_linked_list, last_node
str1 = "ababa"
str2 = "abccba"
str3 = "abcde"
str4 = "abc12de"
str5 = ""
str6 = "v"

first_l1, last_l1  = create_linked_list(str1)
print(search_word(first_l1, first_l1, last_l1))

first_l2, last_l2  = create_linked_list(str2)
print(search_word(first_l2, first_l2, last_l2))

first_l3, last_l3  = create_linked_list(str3)
print(search_word(first_l3, first_l3, last_l3))

first_l4, last_l4  = create_linked_list(str4)
print(search_word(first_l4, first_l4, last_l4))

first_l5, last_l5  = create_linked_list(str5)
print(search_word(first_l5, first_l5, last_l5))

first_l6, last_l6  = create_linked_list(str6)
print(search_word(first_l6, first_l6, last_l6))
